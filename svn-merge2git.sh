#!/bin/sh
# Copyright (c) 2008  Benoit Sigoure <tsuna@lrde.epita.fr>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

OPTIONS_SPEC="\
svn-merge2git [options] [refspec]
--
a,all         Do the work on all local the branches
d,debug       Turn on debug mode (useful if you're hacking the script)
H,no-rewrite  Do not rewrite the entire history (leave the grafts)
m,merge=      Arg of the form: refspec:rev:branch.  Marks refspec as a merge
n,dry-run     Do the entire processing without actually changing anything
p,prune       Clean up everything after rewriting the history (not undo-able!)
P,no-repack   Do not repack the resulting repository (implied by -H)
r,remote      Include remote branches (create a local branch for each of them)
v,verbose     Be more verbose
x,exclude=    refspec to exclude from the potential merge commits
"
SUBDIRECTORY_OK=Yes
. git-sh-setup
cd_to_toplevel

: ${TMPDIR=/tmp}
export TMPDIR

# BRE (Basic RegExp) compatible with `git rev-list --grep' and `sed'.  The RE
# *must* capture the revision merged in its first group.
merge_pattern='[Mm]erge.*[0-9][0-9]*:\([0-9][0-9]*\)'

# BRE which is used to exclude commits whose line that matches of
# $merge_pattern also match this pattern.
exclude_pattern='Finish'

# BRE which is used to exclude matches in the commit log of potential merge
# commits.
log_exclude_pattern='Finish.*merge'

# extract_svn_branch_name <string>
# --------------------------------
# Find the string the name of a SVN branch.  Put the result in
# $svn_branch_name.  Assumes SVN "stdlayout".
extract_svn_branch_name()
{
  case $1 in #(
    '')
      fatal 'extract_svn_branch_name called with empty argument';; #(
    */branches/*)
      extract_svn_branch_name_ 'branches' "$1";; #(
    */tags/*)
      extract_svn_branch_name_ 'tags' "$1"
      warn "found a merge from tag '$svn_branch_name'";; #(
    */trunk*)
      svn_branch_name='trunk';; #(
    *'from the branch "'*'"'*)
      sed_tmp='s/^.*from the branch "//;s/".*$//'
      svn_branch_name="branches/"`echo "$1" | sed "$sed_tmp"`;; #(
    *[Mm]'erge'*[0-9]:[0-9]*' with '*)
      sed_tmp='s/.*with //;s/\.$//'
      svn_branch_name="branches/"`echo "$1" | sed "$sed_tmp"`;; #(
    *[Mm]'erge'*[0-9]:[0-9]*' from '*)
      sed_tmp='s/.*from //;s/\.$//'
      svn_branch_name="branches/"`echo "$1" | sed "$sed_tmp"`;; #(
    *)
      svn_branch_name=;;
  esac
}

# extract_svn_branch_name_ <kind> <string>
# ----------------------------------------
# Helper of extract_svn_branch_name below to factor some code.
# <kind> is probably either 'branches' or 'tags' (for SVN "stdlayout").
# Put the result in $svn_branch_name.
extract_svn_branch_name_()
{
  # XXX: Assumes that a branch name does contain a whitespace.  Fragile.
  # Also strip trailing punctuation.
  sed_tmp="s|.*/\\($1/[^ 	]*\\).*|\\1|;s/[^-a-zA-Z0-9_]*\$//"
  svn_branch_name=`echo "$2" | sed "$sed_tmp"`
}

me=`basename "$0"`

# fatal <msg>
# -----------
# print <msg> on stderr and exit 1
fatal()
{
  die "$me: error: $*"
}

warnings=0
warn_msgs=
# warn <msg>
# ----------
# print <msg> on stderr
warn()
{
  echo "$me: warning: $*" >&2
  warnings=$(($warnings + 1))
  warn_msgs="$warn_msgs
(`date`) warning: $*"
}

# verb <msg>
# ----------
# Print <msg> when verbose mode is enabled.
verb()
{
  $opt_verbose && echo "$*"
}

# debug <msg>
# -----------
# Print <msg> when debug mode is enabled.
debug()
{
  $opt_debug && echo "$*"
}

# find_merge_parent <ref> <merge-line>
# ------------------------------------
# Return (in $merge_parent) the sha1 of the commit that has been merged in by
# <ref>.  <merge-line> must be a line extracted from the commit message of
# <ref> and will be used to extract the SVN revision merged.  For instance, if
# <ref> is a SVN merge of merge-line='Merge -r42:51 in branch foo', this
# function puts the sha1 of the first commit the revision of which is <= 51
# which happens to be in branch foo in $merge_parent.
# If the name of the branch being merged couldn't be found, $merge_parent
# contains 'unknown'.
find_merge_parent()
{
  if $user_defined_merge; then
    sed_tmp="s/^.*@$1:\\([1-9][0-9]*\\):[^@]*@.*\$/\\1/"
    svn_merge_to=`echo "$opt_merge" | sed "$sed_tmp"`
  else
    # Find the first line that matches $merge_pattern, do the substitution and
    # quit.  Ignore all the other lines.
    sed_tmp="s/.*$merge_pattern.*/\\1/"
    svn_merge_to=`echo "$2" | sed "$sed_tmp"`
  fi
  case $svn_merge_to in #(
    '' | 0* | *[^0-9]*)
      fatal "invalid SVN revision '$svn_merge_to' found in $1";;
  esac
  # Now $svn_merge_to is not necessarily a commit that took part of the
  # merge.  For instance, you can merge -r42:51 https://.../branches/foo
  # even if the last commit in branch foo is at r46.  So it's utterly
  # important that we find the last commit on the branch being merged the
  # revision of which must be <= $svn_merge_to (which is 51 in this example).
  if $user_defined_merge; then
    sed_tmp="s/^.*@$1:[^:@]*:\\([^@]*\\)@.*\$/\\1/"
    svn_branch_name=`echo "$opt_merge" | sed "$sed_tmp"`
    test -n "$svn_branch_name" || fatal "internal error in find_merge_parent"
  else
    extract_svn_branch_name "$2"
    if test -z "$svn_branch_name"; then
      merge_parent='unknown'
      return 0
    fi
  fi
  # Create a range to intelligently limit the match of rev-list.  This will
  # produce a RE that rules out all the impossible revision numbers (that is,
  # the revisions >TO).  e.g:
  # 7    -> ([0-7])
  # 42   -> (4[0-2]|[0-3][0-9]|[1-9])
  # 123  -> (12[0-3]|1[0-1][0-9]|0[0-9][0-9]|[1-9][0-9]{0,1})
  # 6951 -> (695[0-1]|69[0-4][0-9]|6[0-8][0-9][0-9]|[0-5][0-9][0-9][0-9]|[1-9][0-9]{0,2})
  perl_tmp='$_ = "'"$svn_merge_to"'";
            my $l = length($_);
            my @r;
            foreach my $i (0 .. $l - 1) {
              /^(\d*)(\d)(\d{$i})$/;
              my ($a, $b, $c) = ($1, int($2), $3);
              if ($i != 0) {
                # Avoid pitfalls e.g. 10[0-9] or 0[0-9][0-9] for 101
                next if $b == 0 or ($b == 1 and $a eq "");
                --$b;
              }
              $b = "[0-$b]" if $b;
              $c =~ s/./[0-9]/g;
              push(@r, "$a$b$c");
            }
            push(@r, "[1-9]" . ($l - 2 ? "[0-9]{0," . ($l - 2) . "}" : ""))
              if $l > 1;
            print "(" . join("|", @r) . ")";'
  rev_range=`perl -we "$perl_tmp"`
  sed_tmp='s/^ *git-svn-id: .*@\([0-9]*\) [-0-9a-f]*$/\1/p'
  svn_merge_parent=`git rev-list --all -1 --header -E \
       --grep="^ *git-svn-id: .*/$svn_branch_name@$rev_range [-0-9a-f]*\\$" \
       | sed -n "$sed_tmp"`
  case $svn_merge_parent in #(
    '' | 0* | *[^0-9]*) fatal "invalid svn_merge_parent: '$svn_merge_parent'";;
  esac
  rv=$?
  test $rv -eq 0 || fatal "perl returned $rv"
  if $opt_verbose; then
    if test "$svn_merge_to" -eq "$svn_merge_parent"; then
      verb_tmp=
    else
      verb_tmp=" (in fact r$svn_merge_parent)"
    fi
  fi
  verb "  $1 is merging SVN r$svn_merge_to$verb_tmp from SVN $svn_branch_name"
  # Now find the sha1 of the merge parent.
  merge_parent=`git rev-list --all \
                 --grep="^ *git-svn-id: .*@$svn_merge_parent [-0-9a-f]*\\$"`
  rv=$?
  test $rv -eq 0 || fatal "git rev-list returned $rv"
}

# create_graft <ref> <merge-parent>
# ---------------------------------
# Add <merge-parent> as 2nd parent of the commit designated by <ref>.
create_graft()
{
  # --parents will print $1 along with its current parents.
  grafted_commit=`git rev-list --no-walk --parents "$1"`
  rv=$?
  test $rv -eq 0 || fatal "git rev-list returned $rv"
  graft_merge_parent=$2

  case $grafted_commit in #(
    *"$graft_merge_parent"*)
      debug "  not grafting commit $1: $graft_merge_parent is already a parent ($grafted_commit)"
      return 0;;
  esac

  graft="$grafted_commit $graft_merge_parent"
  existing_graft=`test -f "$graft_file" && grep "^$1" "$graft_file"`
  if test $? -eq 0; then
    if test x"$existing_graft" != x"$graft"; then
      fatal "$1 is already graft ($existing_graft)\
 and the graft is different than what I was going to graft ($graft)"
    fi
    nalready=$(($nalready + 1))
    debug "  not grafting commit $1: already properly grafted"
    return 0
  fi
  debug "  grafting commit $1: add parent $graft_merge_parent"
  $opt_dryrun && return 0
  nconverted=$(($nconverted + 1))
  echo >>"$graft_file" "$graft" \
  || fatal "Failed to add a graft in $graft_file"
}

# rm_original_refs
# ----------------
# Remove all the refs under refs/original.
rm_original_refs()
{
  if test -f "$GIT_DIR/packed-refs"; then
    sed -i '/refs\/original\//d' "$GIT_DIR/packed-refs" \
      || warn "Failed to edit '$GIT_DIR/packed-refs' (sed returned $?)"
  fi
  rm -rf "$GIT_DIR/refs/original"
}

# rewrite_history
# ---------------
# Make *all* the grafts part of the actual history.
rewrite_history()
{
  filter_branch='git filter-branch --parent-filter cat -- --all'
  if test -n "$parent_unknown"; then
    parent_unknown=`echo "$parent_unknown" | tr ' ' '\\n' | sort -u | xargs`
    echo "I could not find the merge parent or merged branch in the following:
$parent_unknown
If you know which revision they are merging from which branch, you can invoke
me again with --merge <ref>:<rev>:<branch> and I will do the magic for you."
    if $opt_rewrite; then
      echo -n "Do you want to go ahead and rewrite the entire history\
 anyway? [y/N] "
      read answer || {
        warn "failed to read your answer...  I'm not rewriting anything."
        return 1
      }
      case $answer in #(
        [yY]*)
          echo "Alright so I'll rewrite the history now.
Bear in mind that all the refs I printed (especially the ones above of which I
couldn't find the parents) will be (most likely) changed so you'll have to
figure out by yourself what they are to use them --merge"
          echo -n 'Rewriting history in'
          for i in 5 4 3 2 1; do
            echo -n " ... $i"
            sleep 1
          done
          echo ' ... 0'
          ;; #(
        *)
          echo "OK.  Use '$filter_branch' when you're ready or invoke me again."
          return 0;;
      esac
    fi
  fi

  if $opt_rewrite; then
    # Refresh all the timestamps.  I don't know why, they always change with
    # me (only the timestamps!) and git filter-branch will complain because
    # git diff-files will return differences (due to the timestamp change).
    # FIXME: Investigate why this script seems to touch the entire WC.
    # The weird thing is that it works even with a read-only WC.
    git status >/dev/null
    $opt_dryrun && return 0
    if test -f "$graft_file"; then
      $filter_branch || fatal "git filter-branch returned $?"
      rm "$graft_file" || warn "Failed to rm $graft_file"
    else
      warn "No history rewriting necessary"
    fi
    # FIXME: Is it really necessary to repack if we didn't go through the
    # previous `if'?
    if $opt_repack; then
      if $opt_prune; then
        prune='--prune'
        rm_original_refs
      else
        prune=
      fi
      git gc $prune || warn "git gc $prune returned $?"
    fi
  else
    echo "$me: use '$filter_branch' to rewrite the entire history"
  fi
}

# doit <REF>
# ----------
# Find all the merge mentioned in the commit messages and make them become
# real Git merges.
# Commits that are skipped are stored in $skipped.
doit()
{
  refspec=$1
  verb " >> Processing merges in the history of $refspec"

  git rev-list --no-walk "$refspec" >/dev/null \
    || fatal "'$refspec' does not seem to be a valid refspec"

  git rev-list --grep="$merge_pattern" "$refspec" >"$tmp_buf"
  rv=$?
  test $rv -eq 0 || fatal "git rev-list failed and returned $rv"
  while read commit; do
    # Check that we didn't already process this commit.
    grep -F "$commit" "$tmp_done" >/dev/null && continue
    echo "$commit" >>"$tmp_done" || fatal "could not write to $tmp_done"

    case $opt_exclude in #(
      *" $commit "*)
       skipped="$skipped $commit"
       verb "  skipping $commit because it's listed in --exclude";;
    esac

    merge_log=`git log --no-walk "$commit"`
    rv=$?
    test $rv -eq 0 || fatal "git log returned $rv"
    merge_line=`echo "$merge_log" | sed "/$merge_pattern/!d;//q"`

    case $opt_merge in #(
      *"@$commit:"*':'*'@'*)
        user_defined_merge=:;; #(
      *)
        user_defined_merge=false;;
    esac

    # Maybe skip the commit if it matches $exclude_pattern or
    # $log_exclude_pattern (in which case it's not a merge)
    if $user_defined_merge; then
      : # Do not consider this commit for pattern-based exclusion.
    else
      if $has_exclude \
      && echo "$merge_line" | grep -- "$exclude_pattern" >/dev/null; then
        skipped="$skipped $commit"
        verb "  skipping $commit whose log merge-line is: $merge_line"
        continue
      fi
      if $has_log_exclude \
      && echo "$merge_log" | grep -- "$log_exclude_pattern" >/dev/null; then
        skipped="$skipped $commit"
        verb "  skipping $commit whose log is:"
        $opt_verbose && echo "$merge_log" | sed 's/^/  | /'
        continue
      fi
    fi

    nmerge=$(($nmerge + 1))
    verb "  $commit is a merge commit, log says:
  | $merge_line"

    find_merge_parent "$commit" "$merge_line"

    case $merge_parent in #(
      unknown)
        warn "could not find the merge parent of $commit"
        parent_unknown="$parent_unknown $commit"
        continue;; #(
      '' | *[^0-9a-f]*)
        fatal "invalid merge_parent: '$merge_parent'";;
    esac

    create_graft "$commit" "$merge_parent"
    test $? -eq 0 || fatal "failed to create a graft for commit $commit"
  done <"$tmp_buf"
}

# ------------------ #
# `main' starts here #
# ------------------ #

test -d "$TMPDIR" || fatal "TMPDIR='$TMPDIR' is not a directory"
test -w "$TMPDIR" || fatal "TMPDIR='$TMPDIR' is not writable"
tmp_buf=`mktemp "$TMPDIR/$me.XXXXXX"`
tmp_done=`mktemp "$TMPDIR/$me.done.XXXXXX"`
# Clean up temp file upon exit.
trap "exit_status=\$?; rm -f '$tmp_buf' '$tmp_done'; exit \$exit_status" 0

# Parse the options passed to the script.
# Initialize the defaults
opt_all=false
opt_debug=false
opt_dryrun=false
opt_exclude=
# '@'-separated list of ':'-separated triplets (refspec:rev:branch)
opt_merge=
opt_prune=false
opt_remote=false
opt_repack=:
opt_rewrite=:
opt_verbose=false

# -------- #
# `getopt' #
# -------- #
while test $# != 0
do
  case $1 in #(
    -a | --all)
      opt_all=:;; #(
    -d | --debug)
      opt_debug=:;; #(
    -H | --no-rewrite)
      opt_rewrite=false;; #(
    -m | --merge)
      shift
      refspec=${1%%:*}
      test -n `git rev-list --no-walk "$refspec"` \
        || fatal "invalid refspec '$refspec' in --merge argument"
      svn_merge_to=${1%:*}
      svn_merge_to=${svn_merge_to#*:}
      case $svn_merge_to in #(
        '' | 0* | *[^0-9]*)
          fatal "invalid SVN revision '$svn_merge_to' in --merge argument";;
      esac
      svn_branch_name=${1#*:}
      svn_branch_name=${svn_branch_name#*:}
      case $svn_merge_to in #(
        '')
          fatal "empty SVN branch name in --merge argument";; #(
        *' '*)
          fatal "whitespace unsupported in SVN branch name in --merge argument";;
        *'@'*)
          fatal "at (@) unsupported in SVN branch name in --merge argument";;
      esac
      opt_merge="$opt_merge@$1@"
      ;; #(
    -n | --dry-run)
      opt_dryrun=:;; #(
    -p | --prune)
      opt_prune=:;; #(
    -P | --no-repack)
      opt_repack=false;; #(
    -r | --remote)
      opt_remote=:;; #(
    -v | --verbose)
      opt_verbose=:;; #(
    -x | --exclude)
      shift; opt_exclude="$opt_exclude $1";; #(
    --)
      shift; break;; #(
    -*)
      usage;; #(
  esac
  shift
done

# We use rev-list --all a lot.  When we finish, git filter-tree saves all the
# original refs under refs/original.  These will be selected by rev-list --all
# which is something we want to avoid.  So we bail out when refs/original
# exists.
if test -n "`git for-each-ref refs/original`"; then
  if $opt_prune; then
    rm_original_refs
  else
    fatal "There are some refs under refs/original which could be
the refs saved by a previous run of myself.  This can also occur if you used
git filter-branch (which I personally do).  Please get rid of them if you want
to re-run me or re-run me with the --prune options and I'll do it for you."
  fi
fi

if $opt_all; then
  git for-each-ref --shell --format='ref=%(refname)' refs/heads >"$tmp_buf"
  rv=$?
  test $rv -eq 0 || fatal "git for-each-ref failed and returned $rv"
  while read line
  do
    eval "$line"
    set "$@" "$ref"
  done <"$tmp_buf"
fi

if $opt_remote; then
  git for-each-ref --shell --format='ref=%(refname)' refs/remotes >"$tmp_buf"
  rv=$?
  test $rv -eq 0 || fatal "git for-each-ref failed and returned $rv"
  while read line
  do
    eval "$line"
    branch=`basename "$ref"`
    case $branch in #(
      HEAD) # Skip branches named `HEAD' (which does happen)
        continue;; # because they create ambiguities.
    esac
    # if the local $branch does not already exist, we create one
    exists=`git rev-list --no-walk refs/heads/"$branch" 2>/dev/null`
    if test -z "$exists"; then # the $branch does not locally exist
      verb "creating branch '$branch' from '$ref'"
      git branch "$branch" "$ref" \
        || fatal "could not create branch '$branch' from '$ref'"
      set "$@" "refs/heads/$branch"
    else # there already is a local $branch
      sha1=`git rev-list --no-walk "$ref"`
      # Maybe the existing local $branch is identical to the remote $ref?
      if test "$sha1" = "$exists"; then # OK, local = remote
        verb "branch '$branch' is already properly initialized to '$ref'"
        set "$@" "refs/heads/$branch"
      else # KO, local != remote
        warn "there already exists a local branch '$branch'
and it is at $exists whereas the remote branch '$ref'
is at $sha1 so I'm skipping it..."
      fi
    fi
  done <"$tmp_buf"
fi

# No refspec given => work on HEAD
test -z "$*" && set HEAD
graft_file="$GIT_DIR/info/grafts"

if test -z "$exclude_pattern"; then
  has_exclude=false
else
  has_exclude=:
fi

if test -z "$log_exclude_pattern"; then
  has_log_exclude=false
else
  has_log_exclude=:
fi

totalmerge=0
totalconverted=0
nbranch=0
skipped=         # space separated list of commit that we skipped
parent_unknown=  # space separated list of commit of which we couldn't figure
                 # out the merge parent
for refspec
do
  nmerge=0     # number of merges seen for $refspec
  nconverted=0 # number of them we actually grafted
  nalready=0   # number of them that were already grafted/imported
  doit "$refspec"

  if test "$nalready" -eq "0"; then
    alrmsg=
  else
    alrmsg=" ($nalready already converted)"
  fi
  echo ">>> processed $nconverted/$nmerge merges$alrmsg in $refspec"

  totalmerge=$(($totalmerge + $nmerge))
  totalconverted=$(($totalconverted + $nconverted))
  nbranch=$(($nbranch + 1))
done

rewrite_history

if test "$warnings" -ne 0; then
  warn "job completed with $warnings warnings:$warn_msgs"
fi
if test "$totalmerge" -ne "$totalconverted"; then
  skipped=`echo "$skipped" | tr ' ' '\\n' | sort -u | xargs`
  echo "The following commits have been skipped: $skipped"
fi
# FIXME: Print a warning for each unused --merge argument.
echo "Done.  Processed $totalconverted/$totalmerge merges in $nbranch branches"
